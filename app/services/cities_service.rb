class CitiesService
  SERVICE_URL = 'https://search.reservamos.mx/api/v2/places?q='

  attr_writer :filters
  def search(name)
    cities = fetch_cities name
    filter_cities cities
  end

  def fetch_cities(name)
    response = RestClient.get SERVICE_URL + name
    JSON.parse response
  end

  def filter_cities(cities)
    @filters = @filters || {}
    @filters = {} unless @filters.kind_of?(Hash)

    cities.select { |city| @filters.reduce(true) { |val, (k,v)| city[k.to_s] == v && val } }
  end
end
