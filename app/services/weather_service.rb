class WeatherService
  API_KEY = '8eb322fbc9bc74db93867699d0b81654'
  SERVICE_URL = 'https://api.openweathermap.org/data/2.5/onecall?appid=%s&lat=%s&lon=%s&lang=%s&units=%s%s'

  attr_writer :units, :lang, :excluded, :lang

  def excluded
    @excluded = @excluded || [:daily]
    @excluded = [] unless @excluded.kind_of?(Array) or @excluded.kind_of?(String)
    @excluded = [@excluded] if @excluded.kind_of?(String)

    data = [:current, :minutely, :hourly, :daily, :alerts]
    ex = data - @excluded
    '&exclude=' + ex.join(',') if ex.size > 0
  end

  def lang
    @lang || 'es'
  end

  def units
    @units || 'metric'
  end

  def search(lat, lon)
    request = SERVICE_URL % [ API_KEY, lat, lon, lang, units, excluded]
    response = RestClient.get request
    JSON.parse response
  end

end
