class ForecastController < ApplicationController


  def index
    return render status: :bad_request unless params.has_key?('city')

    # {
    #   name: 'sdfjksdf'
    #   slug: 'sdsdsdfsd'
    # }

    forecast = []
    from_city = []
    begin
      cities_service = CitiesService.new
      cities_service.filters = { result_type: 'city', country: 'México' }
      cities = cities_service.search(params['city'])
      if params['from'].present?
        cities_service.filters = { slug: params['from'] }
        from_city = cities_service.search(params['from'])
      end
    rescue => _
      return render status: :service_unavailable, json: { message: 'Cities Service Unavailable' }
    end

    begin
      threads = []
      weather_service = WeatherService.new
      cities.each do |city|
        threads << Thread.new {
          # puts 'Launching thread'
          tempreatures = weather_service.search city['lat'], city['long']
          forecast << {
            city_name: city['city_name'],
            state: city['state'],
            tempreatures: tempreatures['daily'].map { |temp| { min: temp['temp']['min'], max: temp['temp']['max'] } }
          }
        }
      end

      from_city.each do |city|
        threads << Thread.new {
          # puts 'Launching thread'
          tempreatures = weather_service.search city['lat'], city['long']
          forecast << {
            city_name: city['city_name'],
            state: city['state'],
            tempreatures: tempreatures['daily'].map { |temp| { min: temp['temp']['min'], max: temp['temp']['max'] } }
          }
        }
      end

      threads.each { |thread| thread.join } # ; puts "#{thread} complete" }
    rescue => _
      return render status: :service_unavailable, json: { message: 'Weather Service Unavailable' }
    end

    return render json: forecast if forecast.size > 0
    render status: :no_content
  end
end
