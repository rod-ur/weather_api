# Weather API

You must have Ruby installed, then:
```bash
$ gem install rails # install rails cli
$ bundler install # install dependencies
$ rails s # run development server
```
